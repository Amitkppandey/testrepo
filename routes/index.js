var express = require('express');
var router = express.Router();

const clientController = require('../controllers').client;
const orderController = require('../controllers').order;
const butlerController = require('../controllers').butler;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Using Express Framework' });
});

/* --Test 2.2-- */
/* Order Router */
router.get('/order', function(req, res, next){
  res.render('import-order', { title: 'Import Order'});
})
router.post('/order', orderController.import);

/* Client Router */
router.post('/client', clientController.add);

/* --Test 2.1-- */
router.get('/allocate-request', butlerController.allocateAndReport)

module.exports = router;
