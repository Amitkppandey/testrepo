const client = require('./client');
const order = require('./order');
const butler = require('./butler');

module.exports = {
  client,
  order,
  butler
};
