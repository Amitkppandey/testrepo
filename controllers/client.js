const Client = require('../models').Client;
const Order = require('../models').Order;

module.exports = {

  add(req, res) {
    console.log(req.body)
    return Client
      .create({
        clientId: req.body.clientId,
        firstName: req.body.firstName,
        lastName: req.body.lastName
      })
      .then((client) => res.status(201).send(client))
      .catch((error) => res.status(400).send(error));
  },
};
