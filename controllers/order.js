const Client = require('../models').Client;
const Order = require('../models').Order;
const request = require('request');
const csv=require('csvtojson');

module.exports = {

  add(req, res) {
    return Order
      .create({
        orderId: req.body.order_id,
        clientId: req.body.client_id,
        request: req.body.request,
        duration: req.body.duration
      })
      .then((order) => res.status(201).send(order))
      .catch((error) => res.status(400).send(error));
  },

  import(req, res) {
    if(req.body.csvFilePath) {
      csv({
        noheader: false,
        headers: ['orderId', 'clientId', 'requests', 'duration']
      })
      .fromStream(request.get(req.body.csvFilePath))
      .subscribe((jsonData)=>{
        return new Promise((resolve,reject)=>{
          if(jsonData.clientId) {
            var client_id = String(jsonData.clientId).toLowerCase();
            Client.findOne({ where: {clientId: client_id} }).then(client => {
              if(client) {
                console.log(client);
                Order.create({orderId: jsonData.orderId, clientId: jsonData.clientId, request: jsonData.requests, duration: parseInt(jsonData.duration)}).then(() => {
                  console.log(client_id, "This is inserted successfully");
                  return resolve();
                })
              } else {
                console.log(client_id, "This is not inserted");
                return resolve();
              }
            });
          }else {
            console.log("Client id is missing");
            return resolve();
          }
        });
      },() => {
        res.status(402).send("File data has not been imported successfully");
        console.log("File data has not been imported successfully");
      },() => {
        res.status(200).send("File data has been imported successfully");
        console.log("File data has been imported successfully");

      });
    } else {
      res.status(400).send({message: "Please enter correct file path"});
    }
  }
};
