module.exports = {

  allocateAndReport(req, res) {
		const exampleRequests = [
		    {
		        clientId: 1,
		        requestId: 'abc',
		        hours: 6
		    },
		    {
		        clientId: 2,
		        requestId: 'ghi',
		        hours: 1
		    },
		    {
		        clientId: 1,
		        requestId: 'def',
		        hours: 4
		    },
		    {
		        clientId: 1,
		        requestId: 'zzz',
		        hours: 2
		    }
		];
		var returnVal = {
			butlers: [],
			spreadClientIds: []
		}
		for(var i=0; i < exampleRequests.length; i++) {
			if(returnVal.spreadClientIds.indexOf(exampleRequests[i].clientId) === -1) {
				returnVal.spreadClientIds.push(exampleRequests[i].clientId);
			}
			var requestsarr = [];
			for(var j=i+1; j < exampleRequests.length; j++) {
					if((exampleRequests[i].hours + exampleRequests[j].hours) == 8){
						requestsarr.push(exampleRequests[i].requestId);
						requestsarr.push(exampleRequests[j].requestId);
						returnVal.butlers.push({requests: requestsarr});
					}
			}
		}
		res.json(returnVal);
  },
};
