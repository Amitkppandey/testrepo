'use strict';

module.exports = (sequelize, DataTypes) => {
  var Client = sequelize.define('Client', {
    clientId: DataTypes.STRING,
		firstName: DataTypes.STRING,
		lastName: DataTypes.STRING
  }, {});

  Client.associate = function(models) {
    // Client.belongsToMany(models.Order, {
    //   through: 'StudentCourse',
    //   as: 'clients',
    //   foreignKey: 'clientId'
    // });
    // Client.belongsTo(models.Order, {
    //   foreignKey: 'clientId',
    //   as: 'client',
    //   targetKey: 'clientId'
    // });
  };

  return Client;
};
