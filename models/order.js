'use strict';

module.exports = (sequelize, DataTypes) => {
  var Order = sequelize.define('Order', {
    orderId: DataTypes.STRING,
		clientId: DataTypes.STRING,
		request: DataTypes.STRING,
		duration: DataTypes.INTEGER
  }, {});

  Order.associate = function(models) {
    // Order.belongsToMany(models.Client, {
    //   through: 'ClientOrder',
    //   as: 'client',
    //   foreignKey: 'clientId'
    // });
    // Order.belongsTo(models.Client, {
    //   foreignKey: 'clientId',
    //   as: 'client',
    //   sourceKey: 'clientId'
    // });
  };

  return Order;
};
